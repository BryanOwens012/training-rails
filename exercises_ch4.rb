## Ex. 4.2.2

#1:
city = "PA"
state = "CA"

#2:
puts "#{city}, #{state}"

#3:
puts "#{city}\t#{state}"

#4:
puts '#{city}\t#{state}'

## Ex. 4.2.3

#1:
puts "racecar".length

#2:
puts "racecar" == "racecar".reverse

#3:
s = "racecar"
puts s == s.reverse

#4:
puts "It's a palindrome!" if s == s.reverse
s = "onomatopoeia"
puts "It's a palindrome!" if s == s.reverse

## Ex. 4.2.4

#1:
def palindrome_tester(s)
	if s == s.reverse
		puts "It's a palindrome!"
	else
		puts "It's not a palindrome."
	end
end

#2:
palindrome_tester("racecar")
palindrome_tester("onomatopoeia")

#3:
puts palindrome_tester("racecar").nil?

## Ex. 4.3.1

#1:
a = "A man, a plan, a canal, Panama".split(", ")
puts a

#2:
s = a.join("");
puts s

#3:
s = s.split(" ").join("")
puts palindrome_tester(s)
puts palindrome_tester(s.downcase)

#4:
puts ('a'..'z').to_a[7]
puts ('a'..'z').to_a.reverse[7]

## Ex. 4.3.2

#1:
(0..16).each {|i| puts 2**i}

#2:
def yeller(arr)
	puts arr.map { |char| char.upcase }.join()
end
puts yeller(['o', 'l', 'd'])

#3:
def random_subdomain()
	puts ('a'..'z').to_a.shuffle[0..7].join()
end
random_subdomain()

#4:
def string_shuffle(s)
	puts s.split('').shuffle().join()
end
string_shuffle("foobar")

## Ex. 4.3.3

#1:
hash = { 'one' => 'uno', 'two' => 'dos', 'three' => 'tres'}
hash.each do |key, val|
	puts "#{key} in Spanish is #{val}"
end

#2:
person1 = { :first => "p1f", :last => "p1l" }
person2 = { :first => "p2f", :last => "p2l" }
person3 = { :first => "p3f", :last => "p3l" }
params = { :father => person1, :mother => person2, :child => person3 }
puts params[:father][:first]

#3:
hash = { :name => "Bryan", :email => "bryan.owens@yale.edu", "password digest" => ('a'..'z').to_a.shuffle[0..15].join() }
puts hash

#4:
puts ({ "a" => 100, "b" => 200 }.merge({ "b" => 300 }))

## Ex. 4.3.4

## Ex. 4.4.1

#1:
ints = Range.new(1,10)

#2:
ints2 = Range.new(1,10)

#3:
puts ints == ints2

## Ex. 4.4.2

#1:
# Range: Range --> Object --> BasicObject
# Hash: Hash --> Object --> BasicObject
# Symbol: Symbol --> Object --> Basic Object

#2:
class Word < String
	def palindrome?
		self == reverse
	end
end

## Ex. 4.4.3

#1:
class String
	def palindrome?
		self == self.reverse
	end
end

puts "racecar".palindrome?
puts "onomatopoeia".palindrome?
puts "Malayalam".downcase.palindrome?

#2:

class String
	def shuffle
		self.split('').shuffle.join
	end
end
puts "foobar".shuffle

class String
	def shuffle
		split('').shuffle.join
	end
end
puts "foobar".shuffle

## Ex. 4.4.4

## Ex. 4.4.5
